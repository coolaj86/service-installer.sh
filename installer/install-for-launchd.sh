set -u

my_app_launchd_service="Library/LaunchDaemons/${my_app_pkg_name}.plist"

echo ""
echo "Installing as launchd service"
echo ""

# See http://www.launchd.info/
safe_copy_config "$my_app_dist/$my_app_launchd_service" "$my_root/$my_app_launchd_service"

$sudo_cmd chown root:wheel "$my_root/$my_app_launchd_service"

$sudo_cmd launchctl unload -w "$my_root/$my_app_launchd_service" >/dev/null 2>/dev/null
$sudo_cmd launchctl load -w "$my_root/$my_app_launchd_service"

echo "$my_app_name started with launchd"
