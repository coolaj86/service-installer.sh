set -e
set -u

my_name=awesome
# TODO read PROJECT_VERSION and PROJECT_TMP from CLI
my_ver=master
my_tmp=$(mktemp -d)

mkdir -p $my_tmp/opt/$my_name/lib/node_modules/$my_name
git clone https://git.example.com/example/awesome.js.git $my_tmp/opt/$my_name/lib/node_modules/$my_name

echo "Installing to $my_tmp (will be moved after install)"
pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name
  git checkout $my_ver
  source ./installer/install.sh
popd

echo "Installation successful, now cleaning up $my_tmp ..."
rm -rf $my_tmp
echo "Done"
