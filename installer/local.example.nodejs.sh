### IMPORTANT ###
###  VERSION  ###
my_name=goldilocks
my_app_pkg_name=com.daplie.goldilocks.web
my_app_ver="v1.1"
my_azp_oauth3_ver="v1.2"
export NODE_VERSION="v8.9.0"

if [ -z "${my_tmp-}" ]; then
  my_tmp="$(mktemp -d)"
  mkdir -p $my_tmp/opt/$my_name/lib/node_modules/$my_name
  echo "Installing to $my_tmp (will be moved after install)"
  git clone ./ $my_tmp/opt/$my_name/lib/node_modules/$my_name
  pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name
fi

#################
export NODE_PATH=$my_tmp/opt/$my_name/lib/node_modules
export PATH=$my_tmp/opt/$my_name/bin/:$PATH
export NPM_CONFIG_PREFIX=$my_tmp/opt/$my_name
my_npm="$NPM_CONFIG_PREFIX/bin/npm"
#################


my_app_dist=$my_tmp/opt/$my_name/lib/node_modules/$my_name/dist
installer_base="https://git.daplie.com/Daplie/goldilocks.js/raw/$my_app_ver"

# Backwards compat
# some scripts still use the old names
my_app_dir=$my_tmp
my_app_name=$my_name



git checkout $my_app_ver

mkdir -p "$my_tmp/opt/$my_name"/{lib,bin,etc}
ln -s ../lib/node_modules/$my_name/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name
ln -s ../lib/node_modules/$my_name/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name.js
mkdir -p "$my_tmp/etc/$my_name"
chmod 775 "$my_tmp/etc/$my_name"
cat "$my_app_dist/etc/$my_name/$my_name.example.yml" > "$my_tmp/etc/$my_name/$my_name.example.yml"
chmod 664 "$my_tmp/etc/$my_name/$my_name.example.yml"
mkdir -p $my_tmp/srv/www
mkdir -p $my_tmp/var/www
mkdir -p $my_tmp/var/log/$my_name


local_get_deps()
{
  #
  # Dependencies
  #
  echo $NODE_VERSION > /tmp/NODEJS_VER
  http_bash "https://git.coolaj86.com/coolaj86/node-installer.sh/raw/v1.1/install.sh"
  $my_npm install -g npm@4
  pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name
    $my_npm install
  popd
  pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name/packages/assets
    OAUTH3_GIT_URL="https://git.daplie.com/Oauth3/oauth3.js.git"
    git clone ${OAUTH3_GIT_URL} oauth3.org || true
    ln -s oauth3.org org.oauth3
    pushd oauth3.org
      git remote set-url origin ${OAUTH3_GIT_URL}
      git checkout $my_azp_oauth3_ver
      git pull
    popd

    mkdir -p jquery.com
    ln -s jquery.com com.jquery
    pushd jquery.com
      http_get 'https://code.jquery.com/jquery-3.1.1.js' jquery-3.1.1.js
    popd

    mkdir -p google.com
    ln -s google.com com.google
    pushd google.com
      http_get 'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js' angular.1.6.2.min.js
    popd

    mkdir -p well-known
    ln -s well-known .well-known
    pushd well-known
      ln -snf ../oauth3.org/well-known/oauth3 ./oauth3
    popd
    echo "installed dependencies"
  popd
}
