curl-bash-template.sh
=====================

So you've got a neat project and you want to be able to have others get it up and running quickly, eh?

You'd like them to be able to do something like this and have your service installed, eh?

```bash
curl -L install.example.io | bash
```

Well, this is a collection of scripts that you can include in your repo to do just that.

Just modify `get.sh` to point to your repo (you could host this on `install.my-project.io`, for example)
and modify `local.sh` for your specific project.

Currently supports:

	* macOS's launchd
	* Linux's systemd

Make Your Project Layout Like This
----------------------------------

```
/Users/me/git.example.com/me/awesome.js/
├── CHANGELOG
├── LICENSE
├── README.md
├── dist
│   ├── Library
│   │   └── LaunchDaemons
│   │       └── com.example.awesome.plist
│   └── etc
│       ├── awesome
│       │   └── awesome.example.yml
│       ├── systemd
│       │   └── system
│       │       └── awesome.service
│       └── tmpfiles.d
│           └── awesome.conf
├── installer
│   ├── get.sh
│   └── local.sh
├── lib
└── package.json
```

Let Your Followers Install Your Project Like This
----------------------------------

```bash
curl -L https://git.example.com/example/project.git/raw/master/installer/get.sh | bash
```

The installed system looks like this:

```
/
├── etc
│   ├── systemd
│   │   └── system
│   │       └── awesome.service
│   └── tmpfiles.d
│       └── awesome.conf
├── opt
│   └── awesome
│       ├── etc
│       ├── lib
│       └── var
└── Library
    └── LaunchDaemons
        └── com.example.awesome.plist
```

Available Helpers
-----------------

```
$my_root                    typically /, but could have a prefix on android
$sudo_cmd                   'sudo' if not root and sudo is installed, otherwise empty
http_get <url> <filepath>   uses curl or wget to download a file
http_bash <url>             downloads file to a temporary location and runs it with bash
```

# Troubleshooting systemd

## Error 226/Namespace

Most likely a directory that is supposed to be writable doesn't exist.

## Failed to get repository owner (foobar): no such table: user

Your `custom/conf/app.ini` has a line like this:

```
[database]
DB_TYPE = sqlite3
PATH = data/gitea.db
```

And it should probably look like this instead:

```
[database]
DB_TYPE = sqlite3
PATH = /opt/gitea/data/gitea.db
```

## Gitea: Invalid key ID

```
Gitea: Invalid key ID
Invalid key ID[key-2]: public key does not exist [id: 2]
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
```

You are connecting to gitea with a different ssh key (usually `id_rsa.pub`)
than the one you uploaded. You can usually fix this by uploading your default key
or by manually specifying which key to use, for example:

`~/.ssh/config`:
```
Host git.example.com
  User gitea
  IdentityFile ~/.ssh/id_rsa
```

## Expect user 'foobar' but current user is: gitea

```
remote: 2017/10/25 23:53:10 [...s/setting/setting.go:625 NewContext()] [E] Expect user 'aj' but current user is: gitea
remote: error: hook declined to update refs/heads/master
To ssh://git.coolaj86.com:22042/coolaj86/hexdump.js.git
 ! [remote rejected] master -> master (hook declined)
error: failed to push some refs to 'ssh://gitea@git.coolaj86.com:22042/coolaj86/hexdump.js.git'
```

If you copied a previous installation of gitea over to a new user, you may get this error.

I haven't yet found where it comes from, but deleting the repository in the UI and re-adding it seems to do the trick
from what I can tell. Remember to `git fetch --all` first before deleting.

## 203/EXEC

The downloaded gitea file is not executable

```
Oct 28 00:06:19 git-ldsconnect systemd[1]: gitea.service: Main process exited, code=exited, status=203/EXEC
Oct 28 00:06:19 git-ldsconnect systemd[1]: gitea.service: Unit entered failed state.
Oct 28 00:06:19 git-ldsconnect systemd[1]: gitea.service: Failed with result 'exit-code'.
```

Try this:

```bash
sudo chmod +x /opt/gitea/gitea

sudo systemctl restart gitea
```

**But wait, there's more!**

This could also be due to missing `rx` permissions on a parent directory of the executable file, which could be the result of having an existing but empty `/etc/tmpfiles.d/gitea.conf` (a necessary tmp folder not being generated on boot).